const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth');

router.post('/',(req, res)=>{
    
    const username = req.body.username;
    const password = req.body.password;
    authController.login(username, password).then(
        (success)=>{
            res.json(success);
        },
        (err)=>{
            res.status(401).json(err)
        }
     )

    

});

module.exports = router

