const express = require('express');
const router = express.Router();
const _ = require('lodash');
const usuariosControllers = require('../controllers/usuarios');
const mensajeControllers = require('../controllers/mensajes');
const { result } = require('lodash');



router.get('/',(req, res)=>{
  
   console.log("llamado desde modulo de usurios");
    
})

router.post('/', (req, res)=>{
  //metodo create
 const nombres = req.body.nombres;
 const apellidos = req.body.apellidos;
 const nickname = req.body.nickname;
 const email = req.body.email;
 const password = req.body.password;

 usuariosControllers.store(nombres,apellidos,nickname,email,password).then(
    (listaUsuario)=>{
        res.json(listaUsuario);
    },
    ()=>{
     res.status(500).json({message:"Ocurrio un error"});
    }
);

});

router.get('/getAll',(req, res)=>{

    usuariosControllers.list().then(
       async (listaUsuario)=>{
         //listaUsuario.length
            for (var i=0;i<listaUsuario.length;i++){

               let emailContact = listaUsuario[i].email

               var mensajesContact = []
             
               console.log(emailContact)

               x = await usuariosControllers.getLastMessage(emailContact).then(
                   (listLastMessage) => {

                     mensajesContact.push(listLastMessage[0].mensajes[0])
             
                  },
                  (error) => {
                     console.log(error)
                 
                  }

               )

               listaUsuario[i].mensajes = mensajesContact

            }

               res.json(listaUsuario)

       },
       (error)=>{
          res.status(500).json({message: "Ocurrio un error"});
       }

    )

});

router.post('/getUser',(req, res)=>{

   const email_user = req.body.email;

   usuariosControllers.getUser(email_user).then(
      (listaUsuario)=>{

         res.json(listaUsuario);

      },
      (error)=>{
         res.status(500).json({message: "Ocurrio un error"});
      }
   )

});


module.exports = router;