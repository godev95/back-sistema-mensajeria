const express = require('express');
const router = express.Router();
const _ = require('lodash');
const mensajesControllers = require('../controllers/mensajes');

router.post('/listar',(req, res)=>{

   const contact = req.body.email_contact;
   
   const user = req.body.email_user;
   
   mensajesControllers.listar(contact,user).then(

       (listaMensajes)=>{

        res.json(listaMensajes);

       },
       ()=>{
          res.status(500).json({message: "Ocurrio un error"});
       }

    )

})

module.exports = router