const mensajeController = require('../controllers/mensajes');

const sockets = (io) =>{

    io.on('connection', (socket)=>{
        // console.log(socket.broadcast.emit('user connected'));
        // console.log(socket.id);
        socket.on('nuevoMensaje', (data) =>{

            // 

            // var clients = io.sockets.clients();

            // var clients = io.sockets.clients('room');
            // console.log(clients);
            
            // console.log(socket.join(room));
            mensajeController.store(data.mensaje, data.emisor, data.emisor_nombre, data.receptor_nombre, data.receptor);
            
            const emitData = {
                mensaje: data.mensaje,
                emisor: {
                    email: data.emisor,
                    nombre: data.emisor_nombre
                },
                receptor:{
                    email: data.receptor,
                    nombre: data.receptor_nombre
                }
            }

            io.emit('nuevoMensaje', emitData)  


        })


    })



}

module.exports = sockets;