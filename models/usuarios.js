const mongoose = require('mongoose');
const usuariosSchema =new mongoose.Schema({
    nombres: {type:String, required:true},
    apellidos: {type:String},
    email: {type:String, required:true,unique:true, index:true},
    nickname: {type:String, required:true,unique:true},
    photo_profile:{type:String, required:true,unique:true},
    password: {type:String, required:true,index:true},
    create_at: {type: Date, default:new Date()}
})


module.exports = mongoose.model('usuarios',usuariosSchema)