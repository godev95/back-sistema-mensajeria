const mongoose = require('mongoose');

const mensajeSchema = new mongoose.Schema({
    mensaje:{type: String},
    timestamp:{ type: Date, required: true, default: Date.now },
    emisor: {
         email: {type: String},
         nombre:{type: String},
    },
    receptor: {
       email: {type: String},
       nombre:{type: String}        
    },
    estadolectura: {type: String}
})

module.exports = mongoose.model('mensajes', mensajeSchema)