const { reject } = require('lodash');
const _ = require('lodash');
const usuariosModel = require('../models/usuarios');


const Usuarios = {
    list(){
       
        return new Promise((resolve, reject)=>{


            //listar los contactos con el ultimo mensaje no leido del usuario victor flores
            usuariosModel.aggregate(
                [
                     {
                        $lookup:{
                             from:'mensajes',
                             pipeline:[
                              {
                                $match:
                                  {
                                      
                                      $or:[                             
                                         { $and:[{"emisor.email":"victorfq18@gmail.com"},{"receptor.email":"aroman@example.com"} ] },
                                          { $and:[{"emisor.email":"aroman@example.com"},{"receptor.email":"victorfq18@gmail.com"} ] }
                                      
                                      ] 

                                  }
                                                      
                              },
                              { $project: { mensaje: 1, _id: 1,estadolectura:1,timestamp:1 } },
                              { $sort:{"timestamp": -1} },
                              { $limit:1 }
                             // { $count:"mensajes"}
                             ],
                             as:'mensajes'
                        }
                     },
               
                     { $project: { nombres: 1, _id: 1,apellidos:1,photo_profile:1,email:1,mensajes:1 } }
                  
                ]
               ).then(
              (listUser)=>{
                  resolve(listUser)
              },
              (err)=>{
                  reject(err)
              }
          )


        })
        

    },
    getLastMessage(emailContact){
        return new Promise((resolve,reject)=>{

            usuariosModel.aggregate(
                [
                     {
                        $lookup:{
                             from:'mensajes',
                             pipeline:[
                              {
                                $match:
                                  {
                                      
                                      $or:[                             
                                         { $and:[{"emisor.email":"victorfq18@gmail.com"},{"receptor.email":emailContact} ] },
                                          { $and:[{"emisor.email":emailContact},{"receptor.email":"victorfq18@gmail.com"} ] }
                                      
                                      ] 

                                  }
                                                      
                              },
                              { $project: { mensaje: 1, _id: 1,estadolectura:1,timestamp:1 } },
                              { $sort:{"timestamp": -1} },
                              { $limit:1 }
                             // { $count:"mensajes"}
                             ],
                             as:'mensajes'
                        }
                     },
               
                     { $project: { nombres: 1, _id: 1,apellidos:1,photo_profile:1,email:1,mensajes:1 } }
                  
                ]
               ).then(
                     (listLastMessage)=>{
                          resolve(listLastMessage)
                     },
                     (err)=>{
                          reject(err)
                     }
               )

        })
    },
    store(nombres,apellidos,nickname,email,password){
        return new Promise((resolve, reject)=>{
           
           usuariosModel.create(
               { 
                   nombres: nombres,
                   apellidos: apellidos,
                   nickname: nickname,
                   email:email,
                   password:password,
                   photo_profile:"./assets/userdefault.jpg"
               }
           ).then(
               (newUser)=>{
                   resolve(newUser)
               },
               (error)=>{
                   
                   reject(err)
               }
           )
           

        });
  

   },
   getUser(email){

    return new Promise((resolve, reject)=>{

        usuariosModel.find(
            {email: email },
            {nombres:1, apellidos:1, email:1}

        ).then(
            (listUser)=>{
                resolve(listUser)
            },
            (err)=>{
                reject(err)
            }
        )


    })

   }
}

module.exports = Usuarios