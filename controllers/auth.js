const _ = require('lodash');
const usuariosModel = require('../models/usuarios');
const jwt = require('jsonwebtoken');
const Auth = {
     
    login(username, password){
        return new Promise((resolve, reject)=>{
            
            usuariosModel.findOne(
                {email:username, password:password},
                {email:1,nombres:1, apellidos:1},
                (err, user)=>{
                    if(err) return reject(err)
                    if(!user) return reject({mesage: 'Usuario no existe'})
                    const token = jwt.sign({_id:user._id}, 'JavaScript')
                    const newUser = {
                        email: user.email,
                        nombre: user.nombres+' '+user.apellidos,
                        token: token
                    }
                    return resolve(newUser)
               }
            )

        })

    }

}

module.exports = Auth