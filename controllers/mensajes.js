const messageModel = require('../models/mensajes');

const Mensaje = {
    
    store(mensaje,emisor,emisor_nombre,receptor_nombre, receptor){

        
        messageModel.create({
            mensaje: mensaje, 
            emisor: {
                email: emisor,
                nombre: emisor_nombre
            },
            receptor: {
                email: receptor,
                nombre:receptor_nombre
            },
            estadolectura: "1"
        });

     },
     listar(contact,user){

        return new Promise((resolve, reject)=>{
            // db.raffle.find({"$or" : [{"ticket_no" : 725}, {"winner" : true}]})

            messageModel.find(
                {"$or":[{'emisor.email':user,'receptor.email':contact},{'emisor.email':contact,'receptor.email':user}]},
                {mensaje:1, emisor:1, receptor:1, _id:0}
            ).then(
               (listMensajes)=>{
                    resolve(listMensajes);
               },
               (err)=>{
                    reject(err)
               }
            )
        })
        
     },
     countNotificationsContacts(emails){
          
        return new Promise((resolve, reject)=>{

           
            messageModel.find(
                {'emisor.email':{"$in":emails},'receptor.email':'shakira@example.com',estadolectura:'1'},
                {mensaje:1, emisor:1, receptor:1, _id:0}
            ).then(
                (countMensajes)=>{
                    resolve(countMensajes);
                },
                (err)=>{
                     reject(err)
                }
            )

        })
  
     }
}

module.exports = Mensaje